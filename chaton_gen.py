#!/usr/bin/env python3
# coding: utf-8

from url_normalize import url_normalize
import urllib.request as req
import json,ssl,random,re
import argparse


debug=True

parser = argparse.ArgumentParser()
parser.add_argument('--output',type=argparse.FileType('w'),default="myoutput.json", help="JSON output file for list of services")
#,required=True
args=parser.parse_args()

templateurl="https://entraide.chatons.org/page-data/fr/page-data.json"
#templateurl="https://pelican.sainté.net/page-data.json"
# 
#   Base de données actuelle du site entraide
#   Permet de se baser sur les paramètres actifs
#
jsonOutput=args.output.name
#
#   Fichier JSON de description des services du chatons
#   à rendre accessible au téléchargement
#   

print(ssl.OPENSSL_VERSION)
sslctx=ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
sslctx.options |= ssl.OP_NO_SSLv2
sslctx.options |= ssl.OP_NO_SSLv3

try:
    url = url_normalize(templateurl)
except ValueError :
    print("problem url_normalize with %s" % name)
try:
    #resp = req.urlopen(url)
    resp = req.urlopen(url,context=sslctx)
except ValueError:
    print("problem urlopen with %s" % name)
try:
    data = json.loads(resp.read().decode("utf-8"))
except ValueError:
    print("problem jsonloads with %s" % name)
try:
    nodes = data['result']['data']['dataJson']
    example = random.choice(nodes['nodes'])['node']
    apps = data['result']['pageContext']['intl']['messages']
except ValueError:
    print("problem get random example with %s" % name)

listeapp=[]
for k,v in apps.items():
    serv=re.search('service.(.*).title',k)
    if serv :
        listeapp.append({'name': serv.group(1), 'desc': v})
mynode={}
for k,v in example.items():
    mynode[k] = ""

myhelp={}
myhelp['nodes'] = mynode

with open(jsonOutput, 'w') as outfile:
        json.dump(myhelp, outfile)

